# AS-Metrics-Core #

### What is this repository for? ###

* Alphasense Metrics core library is a simple implementation of recording application metrics. The library provides a helper class to record metrics, Add dependecy of respective observability/monitoring tools you want to use (e.g. datadog, signafx etc).
* 0.0.1-SNAPSHOT

### How do I Enable the Metrics Recorder? ###

Add Annotation `@EnableMetrics(commonTags = {"namespace", "application-namespace", "app", "app-name"})` to your Application runner 
and in your code add Java class `MetricsRecorder` dependency in your class where you need.

```xml
@SpringBootApplication
@EnableMetrics(commonTags = {"namespace", "application-namespace", "app", "app-name"})
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
```

### Maven Dependencies ###
```xml
<dependency>
    <groupId>com.alphasense.core.metrics</groupId>
    <artifactId>as-metrics-core</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</dependency>
<!-- Example of adding prometheus dependency to capture the metrics in prometheus format-->
<dependency>
    <groupId>io.micrometer</groupId>
    <artifactId>micrometer-registry-prometheus</artifactId>
    <version>${micrometer.version}</version>
</dependency>

```

### How to call methods to record metrics ###

```xml
metricsRecorder.recordDuration("metrics-name",
        "metrics-description", (long) (Math.random() * 2000),
        Tags.of("tagName", "tagValue"));

//Counter increment, use metricsRecorder.decrementCounter to decrement the counter
metricsRecorder.incrementCounter("metrics-name", "metrics-description",
            Tags.of("tagName", "tagValue"));

// Creating Gauge on the Object
   GaugeMetrics gaugeMetrics = GaugeMetrics.builder().gaugeValue(1).build();

metricsRecorder.recordGuage("metrics-name", gaugeMetrics,
        "metrics-description", Tags.of("tagName", "tagValue"));
gaugeMetrics.setGaugeValue((Math.random() * 100));

//Capturing Distributed Summary.
DistributionSummary summary = metricsRecorder.createDistributedSummary("metrics-name",
        "metrics-description",
        Tags.of("downloader-type", Math.random() > .5 ? "cb" : "owler"));

summary.record((Math.random() * 100));

//Histogram
DistributionSummary summaryHistogram = metricsRecorder.createHistogramDistributedSummary("metrics-name",
        "metrics-description",
        Tags.of("tagName", "tagValue"), 100, new double[]{.5, .95, .99}, new double[]{70, 80, 90});

summaryHistogram.record((Math.random() * 100));
```
### Get acess to MeterRegistry class ###

* In case you have to record something which is not supported in this library you have access to `MeterRegistry` class. Which can be accessed directly by calling `MeterRegistry meterRegistry = metricsRecorder.getMeterRegistry();`

### Who do I talk to? ###

* [Deepak Singh](mailto:dsingh@alpha-sense.com?subject=[GitHub]%20Source%20as-metrics-core)
