package com.alphasense.core.metrics;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = {"com.alphasense.core.metrics"})
public class MetricsConfiguration {
}
