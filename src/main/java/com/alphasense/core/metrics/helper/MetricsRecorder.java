package com.alphasense.core.metrics.helper;

import com.alphasense.core.metrics.GaugeMetrics;
import com.alphasense.core.metrics.MetricsCommonConfig;
import io.micrometer.core.instrument.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@RequiredArgsConstructor
@Component
public class MetricsRecorder {
    @Getter
    private final MeterRegistry meterRegistry;
    private final MetricsCommonConfig metricsConfiguration = MetricsCommonConfig.getInstance();

    public void recordTimeTaken(String metricName, String description, Long startTime) {
        recordTimeTaken(metricName, description, startTime, Tags.empty());
    }

    public void recordTimeTaken(String metricName, String description, Long startTime, Tags tags) {
        Timer.builder(metricName).description(description)
                .tags(tags)
                .tags(metricsConfiguration.getCommonTags())
                .register(meterRegistry)
                .record(System.currentTimeMillis() - startTime, TimeUnit.MILLISECONDS);
    }

    public void recordDuration(String metricName, String description, Long duration, Tags tags) {
        Timer.builder(metricName).description(description)
                .tags(tags)
                .tags(metricsConfiguration.getCommonTags())
                .register(meterRegistry)
                .record(duration, TimeUnit.MILLISECONDS);
    }

    public void incrementCounter(String metricName, String description, Tags tags) {
        Counter.builder(metricName)
                .description(description)
                .tags(tags)
                .tags(metricsConfiguration.getCommonTags())
                .register(meterRegistry)
                .increment();
    }

    public void decrementCounter(String metricName, String description, Tags tags) {
        Counter.builder(metricName)
                .description(description)
                .tags(tags)
                .tags(metricsConfiguration.getCommonTags())
                .register(meterRegistry)
                .increment(-1.0D);
    }

    public void recordGuage(String metricName, GaugeMetrics gaugeMetrics, String description, Tags tags) {
        Gauge.builder(metricName, gaugeMetrics, GaugeMetrics::getGaugeValue)
                .description(description)
                .tags(metricsConfiguration.getCommonTags())
                .tags(tags)
                .register(meterRegistry);
    }

    public DistributionSummary createHistogramDistributedSummary(String metricName, String description, Tags tags, int scale, double[] percentiles, double[] slo) {
        return DistributionSummary
                .builder(metricName)
                .description(description)
                .tags(metricsConfiguration.getCommonTags())
                .tags(tags)
                .publishPercentiles(percentiles)
                .publishPercentileHistogram()
                .serviceLevelObjectives(slo)
                .register(meterRegistry);

    }

    public DistributionSummary createDistributedSummary(String metricName, String description, Tags tags) {
        return DistributionSummary
                .builder(metricName)
                .description(description)
                .tags(metricsConfiguration.getCommonTags())
                .tags(tags)
                .register(meterRegistry);
    }

    public void recordDistributedSummary(DistributionSummary summary, double vaule) {
        summary.record(vaule);
    }

}
