package com.alphasense.core.metrics.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface EnableMetering {
    String name();
    String description() default "";
    String [] tagKeyValues() default {};
    boolean histogram() default false;
    double[] percentiles() default {};

}
