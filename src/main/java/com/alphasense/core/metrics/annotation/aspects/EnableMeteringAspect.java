package com.alphasense.core.metrics.annotation.aspects;

import com.alphasense.core.metrics.annotation.EnableMetering;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tags;
import io.micrometer.core.instrument.Timer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.concurrent.CompletionStage;

@Aspect
@Component
@RequiredArgsConstructor
@Slf4j
public class EnableMeteringAspect {
    private final MeterRegistry meterRegistry;

    public static final String DEFAULT_METRIC_NAME = "method.enablemetring";
    public static final String DEFAULT_EXCEPTION_TAG_VALUE = "none";
    public static final String EXCEPTION_TAG = "exception";
    private static final Logger logger = LoggerFactory.getLogger(EnableMeteringAspect.class);

    @Around("@annotation(com.alphasense.core.metrics.annotation.EnableMetering)")
    public Object publishMetrics(ProceedingJoinPoint pjp) throws Throwable {
        Method method = ((MethodSignature)pjp.getSignature()).getMethod();
        EnableMetering enableMetering = method.getAnnotation(EnableMetering.class);
        if (enableMetering == null) {
            method = pjp.getTarget().getClass().getMethod(method.getName(), method.getParameterTypes());
            enableMetering = method.getAnnotation(EnableMetering.class);
        }

        final boolean stopWhenCompleted = CompletionStage.class.isAssignableFrom(method.getReturnType());

        return processWithTimer(pjp, enableMetering, stopWhenCompleted);
    }


    private Object processWithTimer(ProceedingJoinPoint pjp, EnableMetering enableMetering, boolean stopWhenCompleted) throws Throwable {

        Timer.Sample sample = Timer.start(meterRegistry);
        final String metricName = enableMetering.name().isEmpty() ? DEFAULT_METRIC_NAME : enableMetering.name();

        if (stopWhenCompleted) {
            try {
                return ((CompletionStage<?>) pjp.proceed()).whenComplete((result, throwable) ->
                        record(pjp, enableMetering, metricName, sample, getExceptionTag(throwable)));
            } catch (Exception ex) {
                record(pjp, enableMetering, metricName, sample, ex.getClass().getSimpleName());
                throw ex;
            }
        }

        String exceptionClass = DEFAULT_EXCEPTION_TAG_VALUE;
        try {
            return pjp.proceed();
        } catch (Exception ex) {
            exceptionClass = ex.getClass().getSimpleName();
            throw ex;
        } finally {
            record(pjp, enableMetering, metricName, sample, exceptionClass);
        }
    }

    private void record(ProceedingJoinPoint pjp, EnableMetering enableMetering, String metricName, Timer.Sample sample, String exceptionClass) {
        try {
            sample.stop(Timer.builder(metricName)
                    .description(enableMetering.description().isEmpty() ? null : enableMetering.description())
                    .tags(enableMetering.tagKeyValues())
                    .tags(EXCEPTION_TAG, exceptionClass)
                    .tags(Tags.of("class", pjp.getStaticPart().getSignature().getDeclaringTypeName(),
                            "method", pjp.getStaticPart().getSignature().getName()))
                    .publishPercentileHistogram(enableMetering.histogram())
                    .publishPercentiles(enableMetering.percentiles().length == 0 ? null : enableMetering.percentiles())
                    .register(meterRegistry));
        } catch (Exception e) {
            logger.error("Unable to create and publish metrics", e);

        }
    }

    private String getExceptionTag(Throwable throwable) {
        if (throwable == null) {
            return DEFAULT_EXCEPTION_TAG_VALUE;
        }
        if (throwable.getCause() == null) {
            return throwable.getClass().getSimpleName();
        }

        return throwable.getCause().getClass().getSimpleName();
    }

}
