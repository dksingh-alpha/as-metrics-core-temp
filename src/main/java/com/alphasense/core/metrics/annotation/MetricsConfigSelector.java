package com.alphasense.core.metrics.annotation;

import com.alphasense.core.metrics.MetricsCommonConfig;
import com.alphasense.core.metrics.MetricsConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;

@Configuration
@ComponentScan(basePackages = {"com.alphasense.core.metrics"})
public class MetricsConfigSelector implements ImportSelector {
    @Override
    public String[] selectImports(AnnotationMetadata annotationMetadata) {
        AnnotationAttributes attributes = this.getAttributes(annotationMetadata);

        String [] commonTags = attributes.getStringArray("commonTags");
        MetricsCommonConfig metricsConfiguration = MetricsCommonConfig.getInstance();
        metricsConfiguration.setCommonTags(commonTags);
        return new String[]{MetricsConfiguration.class.getName()};
    }



    private AnnotationAttributes getAttributes(AnnotationMetadata metadata) {
        AnnotationAttributes attributes = AnnotationAttributes.fromMap(metadata.getAnnotationAttributes(EnableMetrics.class.getName(), true));
        Assert.notNull(attributes, () -> "No auto-configuration attributes found. Is " + metadata.getClassName() + " annotated with " + ClassUtils.getShortName(EnableMetrics.class.getName()) + "?");
        return attributes;
    }
}
