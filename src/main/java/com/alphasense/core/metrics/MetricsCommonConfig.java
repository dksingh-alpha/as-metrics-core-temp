package com.alphasense.core.metrics;

import lombok.Getter;
import lombok.Setter;

public class MetricsCommonConfig {
    @Getter
    @Setter
    String [] commonTags;
    private static MetricsCommonConfig configuration = new MetricsCommonConfig();
    private MetricsCommonConfig(){
    }

    public static MetricsCommonConfig getInstance(){
        return configuration;
    }
}
