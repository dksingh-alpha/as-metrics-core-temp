package com.alphasense.core.metrics;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class GaugeMetrics {
    double gaugeValue;
}
